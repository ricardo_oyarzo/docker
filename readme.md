# Desafío
## Run APP

ir a carpeta 

    Backend/docker

modificar el archivo 

    development.yml 

Agregar los datos correspondientes a un correo electronico

Modificar el usuario que se creara en la base de datos, por defecto el archivo con el INSERT esta en:

    mysql\init_tabla_usuario.sql

luego ejecutar los 4 proyectos con docker-composer (desde la carpeta donde esta el archivo development.yml: Backend/docker)

    docker-compose.exe -f .\development.yml up -d


Nota: la url del backend se establece en el proyecto del front end en:

    constants.js

    export const SERVER_URL = 'http://localhost:8002/'
    export const SERVER_REINICIO = 'http://localhost:8001/'
---

## Funcionalidades


luego acceder a

  [http://127.0.0.1:3000](http://127.0.0.1:3000)

Hacer click en:

¿Ha olvidado su contraseña?.

lo que llevara a la ruta:

  [http://127.0.0.1:3000/reiniciarpassword](http://127.0.0.1:3000/reiniciarpassword)

ingresar el correo de un usuario en la base de datos

el sistema redireccionara al 

  [http://127.0.0.1:3000/login](http://127.0.0.1:3000/login)

enviara un correo electronico con un link:

  [http://localhost:3000/cambiarpassword/tokendevalidacion](http://localhost:3000/cambiarpassword/tokendevalidacion)

ir a la rfuta indficada (haciendo click al link del correo)

ingresar una nueva password

si todo esta correcto, aparecera un mensaje de cambio exitoso y sera redireccionado a kla pagina de login:

  [http://127.0.0.1:3000/login](http://127.0.0.1:3000/login)

Ingresar la nueva contraseña y se realizara la autentificación por medio de JWT.

Pudiendo ver el contenido protegido:

"Bienvenido querido usuario al Banco Desafío :)"

---


Fin.-